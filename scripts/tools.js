var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('./store.db');

module.exports = {
    'get_channels': function(callback) {
        db.all("SELECT rowid, name FROM channels", [], function(err, rows) {
            callback(rows || []);
        });
    },
    'get_channel': function(id, callback) {
        db.get("SELECT name FROM channels WHERE rowid=?", [id], function(err, row) {
            callback(row.name);
        });
    },
    'get_messages': function(id, callback) {
        console.log(id);
        db.all("SELECT a.rowid, a.text, a.time, b.name FROM messages a\
                                LEFT JOIN users b ON a.user_id = b.rowid\
                                 WHERE chan=?", [id], function(err, rows) {
            callback(err, rows || []);
        });
    },
    'new_channel': function(name, callback) {
        if (!name) {
            callback('Name is empty');
        } else {
            db.run("INSERT INTO channels (name) VALUES (?)", [name], function(err) {
                callback(err);
            });
        }
    },
    'new_message': function(channel, text, id, callback) {
        if (!text) {
            callback('Message is empty');
        } else if (channel && id){
            db.run("INSERT INTO messages (chan, text, time, user_id)\
                    VALUES (?, ?, ?, ?)", [channel, text, Date.now(), id], function(err) {
                callback(err);
            });
        } else {
            callback('Invalid request');
        }
    }
};
