var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('./store.db');
var tools = require('./tools.js');

module.exports = function(data, socket){
    socket.handshake.session.userdata = socket.handshake.session.userdata || {};
    socket.handshake.session.userdata.name = data.name;
    if (data.new_user) {
        if (data.password != data.password_conf) {
            socket.emit('validated', {
                'warn': 'Passwords don\'t match'
            });
            return;
        }
        try {
            db.run("INSERT INTO users (name, password) VALUES (?, ?)", data.name, data.password);
        } catch (e) {
            socket.emit('validated', {
                'warn': 'User exists'
            });
        }
    }

    db.get("SELECT rowid, name FROM users WHERE name=? AND password=? LIMIT 1", data.name, data.password, function(err, row) {
        if (!err && row) {
            socket.handshake.session.userdata = socket.handshake.session.userdata || {};
            socket.handshake.session.userdata.name = row.name;
            socket.handshake.session.userdata.id = row.rowid;
            socket.handshake.session.save();
            socket.emit('validated', {
                'name': row.name
            });
            tools.get_channels(function(chans) {
                socket.emit('channels', {
                    'chans': chans
                });
            });
        } else {
            socket.emit('validated', {
                'warn': 'Username or password incorrect',
                'log': err
            });
        }
    });
};
