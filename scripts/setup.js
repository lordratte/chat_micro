var sqlite3 = require('sqlite3').verbose();
var fs = require('fs');

var db_path = './store.db';

fs.existsSync(db_path) && fs.unlinkSync(db_path);
var db = new sqlite3.Database(db_path);

db.serialize(function() {
    db.run("CREATE TABLE users (name PRIMARY KEY, password)");
    db.run("CREATE TABLE channels (name PRIMARY KEY)"); //More uses later
    db.run("CREATE TABLE messages (user_id, time, text, chan,\
                            FOREIGN KEY (user_id) REFERENCES users (rowid),\
                            FOREIGN KEY (chan) REFERENCES channels (rowid))");
    db.run("INSERT INTO channels (name) VALUES (?)", ['General']);
    db.run("INSERT INTO channels (name) VALUES (?)", ['Random']);
});
