var chan = null;
var socket = null;
var messages = {};
socket = io();

function commands(data) {
    if (data.warn) {
        Materialize.toast(data.warn, 4000);
    }
    if (data.log) {
        console.log(data.log);
    }
}

function filter(keys, query) {
    for (var k in keys) {
        messages[keys[k]].time = new Date(messages[keys[k]].time);
        msgs.push(messages[keys[k]]);
    }
}


function manifest_messages() {
    var keys = Object.keys(messages).sort(function(a, b){return a-b});
    msgs = [];
    for (var k in keys) {
        messages[keys[k]].time = new Date(messages[keys[k]].time);
        if (!$('#search').val()
                || messages[keys[k]].text.toLowerCase()
                        .indexOf($('#search').val().toLowerCase()) !== -1) {
            msgs.push(messages[keys[k]]);
        }
    }
    $("#msgs").loadTemplate('templates/msg_item.html', msgs);
}

socket.on('validated', function(data){
    if (data.name) {
        $('#login').modal('close');
        $('#user_name').text(data.name);
        $('.overlay').hide();
    } else {
        $('#login').modal('open');
    }
    commands(data);
});

socket.on('action', function(data){
    switch (data.action) {
        case 'refresh':
            location.reload();
        break;
        case 'scroll':
            $("html, body").animate({ scrollTop: $("#main").height()}, 1000);
        break;
    }
    commands(data);
});

socket.on('channels', function(data){
    for (var c in data.chans) {
        data.chans[c].href = "#action/change_chan?i="+data.chans[c].rowid;
    }
    $("#chans").loadTemplate('templates/chan_item.html', data.chans);
    commands(data);
});

socket.on('messages', function(data){
    for (var m in data.messages) {
        messages[data.messages[m].rowid] = data.messages[m];
    }
    manifest_messages();
    commands(data);
});

socket.on('inbox', function(c){
    if (c == chan) {
        messages = {};
        socket.emit('change_chan', {
            'id': chan,
            'scrollme': false
        });
    }
});

socket.on('channel_name', function(data){
    $('#channel_name').text(data.name ? '—'+data.name : '');
    commands(data);
});

$(document).ready(function() {
    $('#make_chan').modal();
    $('#login').modal({
        'dismissible': false
    });
    Router.navigate('#');
    Router.init();
    $('#new_user').click(function() {
        if (this.checked) {
            $('#confirm_sec').show();
        } else {
            $('#confirm_sec').hide();
        }
    });
    $(document).keypress(function(e) {
        if(e.which == 13 && $('#input').val()) {
            if (chan) {
                $("html, body").animate({ scrollTop: $("#main").height()}, 1000);
                var text = $('#input').val();
                $('#input').val('');
                socket.emit('message', {
                    'channel': chan,
                    'text': text
                });
            } else {
                Materialize.toast('Pick a channel to begin!', 4000);
            }
        }
    });
    $('#search').on('input', function() {
        manifest_messages();
    });
    socket.emit('validate');
});

Router.add({
    'path': '#action/:ref',
    'on': function(){
        switch (this.params.ref) {
            case 'login':
                socket.emit('login', {
                    'name': $('#name').val(),
                    'password': $('#password').val(),
                    'password_conf': $('#password_conf').val(),
                    'new_user': $('#new_user').prop('checked')
                });
            break;
            case 'logout':
                socket.emit('logout');
            break;
            case 'make_chan':
                socket.emit('make_chan', $('#chan_name').val());
            break;
            case 'change_chan':
                chan = this.query.i;
                messages = {};
                socket.emit('change_chan', {
                    'id': this.query.i,
                    'scrollme': false
                });
            break;
        }
        Router.navigate('#');
    }
});
