var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var ios = require('socket.io-express-session');
var SessionStore = require('session-file-store')(session);

var login = require('./scripts/login.js');
var tools = require('./scripts/tools.js');

app.set('port', (process.env.NODE_PORT || 5000));
app.set('ip', (process.env.NODE_IP || "0.0.0.0"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

var sess = session({
    secret: 'oHXPKoz5oNXBqIC6KbyP4OJj03mVNbFSq3gLwIBqmS5DgxpIskR2cUdIzf55',
    resave: true,
    saveUninitialized: true,
    cookie: {},
    store: new SessionStore({path: __dirname+'/tmp/sessions'})
});

io.use(ios(sess));
app.use(sess);


app.use(express.static(__dirname + '/www', {index:'main.html'}));

io.on('connection', function(socket){
    try {
        console.log(socket.handshake.session.userdata.name+' connected');
    } catch (e) {
        console.log(socket.handshake.sessionID+' connected');
    }
    socket.on('disconnect', function(){
        console.log('user disconnected');
    });

    socket.on('validate', function(){
        data = socket.handshake.session.userdata;
        if (data) {
            socket.emit('validated', {
                'name': data.name
            });
            tools.get_channels(function(chans) {
                socket.emit('channels', {
                    'chans': chans
                });
            });
        } else {
            socket.emit('validated', {});
        }
    });

    socket.on('make_chan', function(name){
        tools.new_channel(name, function(err) {
            socket.emit('action', {
                'warn': err
            });
            tools.get_channels(function(chans) {
                socket.emit('channels', {
                    'chans': chans
                });
            });
        });
    });
    socket.on('change_chan', function(data){
        tools.get_channel(data.id, function(name) {
            socket.emit('channel_name', {
                'name': name
            });
        });
        tools.get_messages(data.id, function(err, messages) {
            socket.emit('messages', {
                'messages': messages
            });
            if (data.scrollme) {
                socket.emit('action', {
                    'action': 'scroll'
                });
            }
        });
    });

    socket.on('message', function(data){
        tools.new_message(data.channel,
                            data.text,
                            (socket.handshake.session.userdata
                            || {}).id,
                            function(err, messages) {
            socket.emit('action', {
                'warn': err
            });
            tools.get_messages(data.channel, function(err, messages) {
                socket.broadcast.emit('inbox', data.channel);
                socket.emit('action', {
                    'log': err
                });
                socket.emit('messages', {
                    'messages': messages,
                    'log': messages
                });
            });
        });
    });

    socket.on('login', function(data){
        login(data, socket);
    });
    socket.on('logout', function(data){
        delete socket.handshake.session.userdata;
        socket.handshake.session.save();
        socket.emit('action', {
            'action': 'refresh'
        });
    });
});

http.listen(app.get('port'), function(){
  console.log('listening on *:'+app.get('port'));
});
